export type PlayerStateResponse = {
  id: string;
  name: string;
  team?: 'blue' | 'red';
  is_ready: boolean;
  is_captain: boolean;
}

export type MeStateResponse = {
  id: string;
  name?: string;
  team?: 'blue' | 'red';
  is_ready: boolean;
  is_captain: boolean;
}

export type Word = {
  name: string;
  team: 'blue' | 'red' | 'DANGER' | null;
  is_open: boolean;
}

export type GameStateResponse = {
  id: string;
  status: 'LOBBY' | 'STARTED' | 'LOADING' | 'COMPLETED';
  words: Word[];
  players: PlayerStateResponse[];
  me: MeStateResponse;
  message: string;
  is_game_ready: boolean;
  winner: 'blue' | 'red' | null;
  team_score: {
    blue: number;
    red: number;
  }
  remaining_word_count: {
    blue: number;
    red: number;
  }
}
