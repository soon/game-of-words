import * as React from "react";
import LobbyContent, {LobbyContentProps} from '../../components/lobby';
import GameBoardContent, {GameBoardContentProps} from "../../components/game-board";
import {useEffect} from "react";
import {useRouter} from "next/router";
import FinishedGameContent from "../../components/finished-game/finished-game";

type GameProps = LobbyContentProps & GameBoardContentProps & {
  onStartNewGame(copyGameId?: string): void;
  onConnectToGame(gameId: string): void;
};

export default function Game(props: GameProps) {
  const router = useRouter();

  useEffect(() => {
    if (router.query.gameId) {
      props.onConnectToGame(router.query.gameId as string)
    }
  }, [router.query.gameId])

  switch (props.gameState.status) {
    case "LOADING":
      return <div/>
    case 'LOBBY':
      return <LobbyContent {...props}/>
    case 'STARTED':
      return <GameBoardContent {...props}/>
    case 'COMPLETED':
      return <FinishedGameContent {...props}/>
  }
  return null;
}
