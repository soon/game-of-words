import * as React from "react";
import GameBoardContent from "../components/game-board";

export default function GameBoard(props) {
  return <GameBoardContent {...props}/>
}
