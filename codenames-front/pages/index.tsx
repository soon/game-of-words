import React from "react";
import StartGameContent from "../components/start-game";

export default function Index(props) {
  return <StartGameContent
    title='Игра в ассоциации'
    buttonTitle='Начать Игру'
    {...props}
  />
}
