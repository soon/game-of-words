import {Snackbar} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import MuiAlert from '@material-ui/lab/Alert';
import {useRouter} from 'next/router';
import React, {Dispatch, SetStateAction, useDebugValue, useMemo, useRef, useState} from 'react';
import {Options} from 'react-use-websocket/dist/lib/types';
import {useWebSocket} from 'react-use-websocket/dist/lib/use-websocket';
import DebugView, {Message} from '../components/debug-view';
import Layout from "../components/layout";
import {lightOrange} from "../components/palette/light-orange";
import {softRed} from "../components/palette/soft-red";
import {GameStateResponse} from '../types';

import './reset.scss'

const theme = createMuiTheme({
  palette: {
    primary: lightOrange,
    secondary: softRed,
  },
});

const NEW_GAME_ACTION = 'new-game';
const NEW_USER_ACTION = 'new-user';
const AUTH_ACTION = 'auth';
const GAME_UPDATE_RESPONSE_ACTION = 'game-update';
const GENERATE_NEW_NAME_ACTION = 'generate-new-name';
const SET_TEAM_ACTION = 'set-team';
const SET_NAME_ACTION = 'set-name';
const CONNECT_TO_GAME_ACTION = 'connect-to-game';
const SET_CAPTAIN_ACTION = 'set-captain';
const SET_READY_ACTION = 'set-ready';
const START_GAME_ACTION = 'start-game';
const GUESS_WORD_ACTION = 'guess-word';

const DEBUG = process.env.DEBUG;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function useStateWithLabel<S>(initialValue: S | (() => S), name: string): [S, Dispatch<SetStateAction<S>>] {
  const [value, setValue] = useState<S>(initialValue);
  useDebugValue(`${name}: ${value}`);
  return [value, setValue];
}

type WsState = {
  connectToGameId: string | null;
  gameState: GameStateResponse;
  currentUrl: string;
};

export default function MyApp(props) {
  const {Component, pageProps} = props;
  const [notification, setNotification] = React.useState<{ text: string; severity: 'error' | 'warning' } | null>(null);
  const [debugMessages, setDebugMessages] = React.useState<Message[]>([]);
  const router = useRouter();

  const [gameState, setGameState] = React.useState<GameStateResponse>({
    id: 'initial',
    status: 'LOADING',
    players: [],
    words: [],
    me: {
      id: 'initial',
      is_ready: false,
      is_captain: false,
    },
    message: '',
    is_game_ready: false,
    winner: null,
    team_score: {
      blue: 0,
      red: 0,
    },
    remaining_word_count: {
      blue: 0,
      red: 0,
    }
  });
  const [connectToGameId, setConnectToGameId] = useStateWithLabel<string | null>(null, 'connectToGameId');
  const [didAuth, setDidAuth] = useStateWithLabel<boolean>(false, 'didAuth');
  const [name, setName] = useState('');

  const wsStateRef = useRef<WsState>(null);
  wsStateRef.current = {
    connectToGameId: connectToGameId,
    gameState: gameState,
    currentUrl: router.route,
  };

  const handleDidAuth = () => {
    setDidAuth(true);
    if (wsStateRef.current.connectToGameId) {
      sendMessageJson({
        action: CONNECT_TO_GAME_ACTION,
        game_id: wsStateRef.current.connectToGameId
      })
    }
  };

  const addInDebugMessage = msg => {
    setDebugMessages(messages => messages.concat({
      kind: 'in',
      content: msg
    }));
  };
  const addOutDebugMessage = msg => {
    setDebugMessages(messages => messages.concat({
      kind: 'out',
      content: msg
    }));
  };

  const handleWsMessage = async message => {
    addInDebugMessage(message);

    const payload = JSON.parse(message);
    if (payload.status === 'error') {
      setNotification({
        text: payload.message || 'An error has occurred',
        severity: 'error'
      });
      if (payload.code == 'game.not-found') {
        await router.push('/')
      }
      return;
    }
    const action = payload.action;
    if (action == null) {
      console.warn(`Unknown message: ${message}`);
      return;
    }

    const handlers = {
      [AUTH_ACTION]: handleAuthResponse,
      [NEW_USER_ACTION]: handleNewUserResponse,
      [NEW_GAME_ACTION]: handleNewGameResponse,
      [GAME_UPDATE_RESPONSE_ACTION]: handleGameUpdate,
      [GENERATE_NEW_NAME_ACTION]: handleGenerateNewUsernameResponse,
    };
    const handler = handlers[action];
    if (handler == null) {
      console.warn(`Unknown message action: ${message}`);
      return;
    }
    handler(payload);
  };

  const STATIC_OPTIONS = useMemo<Options>(() => ({
    shouldReconnect: () => true,
    onMessage: message => handleWsMessage(message.data),
    onOpen: () => {
      const userId = localStorage.getItem('userId');
      if (userId && userId !== 'null' && userId !== 'undefined') {
        sendMessageJson({action: AUTH_ACTION, user_id: userId});
      } else {
        sendMessageJson({action: NEW_USER_ACTION});
      }
    }
  }), []);
  const [sendMessage] = useWebSocket(process.env.WS_URL, STATIC_OPTIONS);
  const sendMessageJson = data => {
    const message = JSON.stringify(data);
    addOutDebugMessage(message);
    sendMessage(message);
  };

  const handleStartNewGame = (copyGameId?: string) => {
    const payload = {action: NEW_GAME_ACTION};
    if (copyGameId) {
      payload['copy'] = copyGameId;
    }
    sendMessageJson(payload);
  }

  const handleAuthResponse = () => {
    handleDidAuth()
  };

  const handleNewUserResponse = (message) => {
    localStorage.setItem('userId', message['user_id']);
    handleDidAuth()
  };

  const handleNewGameResponse = async ({game}: { game: GameStateResponse }) => {
    setGameState(game);
    setNotification({
      text: game.message,
      severity: 'warning',
    });
    await router.push(`/game/[gameId]`, `/game/${game.id}`);
  };

  const handleGameUpdate = async ({game}: { game: GameStateResponse }) => {
    if (wsStateRef.current.gameState.id === 'initial' || game.id === wsStateRef.current.gameState.id) {
      setGameState(game);
      setNotification({
        text: game.message,
        severity: 'warning'
      });
      if (game.status === 'STARTED') {
        await router.push('/game/[gameId]', `/game/${game.id}`);
      }
    }
  };

  const handleGenerateNewUsernameResponse = async ({name}) => {
    setName(name);
  }

  const handleGenerateNewUsername = () => {
    sendMessageJson({
      action: GENERATE_NEW_NAME_ACTION,
      game_id: wsStateRef.current.gameState.id,
    });
  };

  const handleSetTeam = team => {
    sendMessageJson({
      action: SET_TEAM_ACTION,
      game_id: wsStateRef.current.gameState.id,
      team: team,
    });
  };

  const handleSetName = name => {
    setName(name);
    sendMessageJson({
      action: SET_NAME_ACTION,
      game_id: wsStateRef.current.gameState.id,
      name: name,
    });
  };

  const handleSetCaptain = (isCaptain) => {
    sendMessageJson({
      action: SET_CAPTAIN_ACTION,
      game_id: wsStateRef.current.gameState.id,
      is_captain: isCaptain,
    })
  };

  const onSetReady = (isReady) => {
    sendMessageJson({
      action: SET_READY_ACTION,
      game_id: wsStateRef.current.gameState.id,
      is_ready: isReady
    })
  };

  const handleConnectToGame = (gameId: string) => {
    if (didAuth) {
      sendMessageJson({
        action: CONNECT_TO_GAME_ACTION,
        game_id: gameId
      })
    } else {
      setConnectToGameId(gameId)
    }
  };

  const handleStartGame = () => {
    sendMessageJson({
      action: START_GAME_ACTION,
      game_id: wsStateRef.current.gameState.id
    })
  };

  const handleGuessWord = (word) => {
    sendMessageJson({
      action: GUESS_WORD_ACTION,
      game_id: wsStateRef.current.gameState.id,
      word: word
    })
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setNotification(null);
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <Layout>
          <Component
            onStartNewGame={handleStartNewGame}
            onGenerateNewUsername={handleGenerateNewUsername}
            onSetTeam={handleSetTeam}
            onSetCaptain={handleSetCaptain}
            onSetReady={onSetReady}
            name={name}
            onSetName={handleSetName}
            onConnectToGame={handleConnectToGame}
            onStartGame={handleStartGame}
            onGuessWord={handleGuessWord}
            gameState={gameState}
            {...pageProps}
          />
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            open={notification != null && notification.text != null && notification.text !== ''}
            onClose={handleCloseNotification}
          >
            {notification && <Alert onClose={handleCloseNotification} severity={notification.severity}>
              {notification.text}
            </Alert>}
          </Snackbar>
          {DEBUG && <DebugView messages={debugMessages}/>}
        </Layout>
      </ThemeProvider>
    </>
  );
}