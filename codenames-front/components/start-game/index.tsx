import React from "react";
import GradientBtn from "../start-game-btn";
import css from './style.module.scss';

export type StartGameContentProps = {
  title: React.ReactNode
  buttonTitle: string
  onStartNewGame(copyGameId?: string): void
}

export default function StartGameContent({title, buttonTitle, onStartNewGame}: StartGameContentProps) {
  return (
    <div className={css['start-game-container']}>
      <h1 className={css['main-title']}>{title}</h1>
      <GradientBtn className={css['start-game-button']} onClick={() => onStartNewGame()}>{buttonTitle}</GradientBtn>
    </div>
  )
}