import React from "react";
import {GameStateResponse} from '../../types';
import GameBoardContent from '../game-board';
import GradientBtn from '../start-game-btn';
import css from './style.module.scss';

type FinishedGameContentProps = {
  gameState: GameStateResponse;
  onStartNewGame(copyGameId?: string): void;
}


export default function FinishedGameContent({gameState, onStartNewGame}: FinishedGameContentProps) {
  return (
    <div className={css.container}>
      <h1>Игра окончена</h1>
      <h2>Победила команда {gameState.winner === 'red' ? 'Красных' : 'Синих'}</h2>
      <GradientBtn className={css['start-game-button']} onClick={() => onStartNewGame()}>
        Новая игра
      </GradientBtn>
      <GradientBtn className={css['start-game-button']} onClick={() => onStartNewGame(gameState.id)}>
        Новая игра с теми же игроками
      </GradientBtn>
      <h2>Карта игры:</h2>
      <GameBoardContent gameState={gameState} onGuessWord={() => null}/>
    </div>
  )
}