import React, {PropsWithChildren} from "react";
import css from './style.module.scss';

export default function Layout({children}: PropsWithChildren<{}>) {
  return <div className={css.layout}>{children}</div>
}