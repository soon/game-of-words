import {Container, Grid, Paper} from "@material-ui/core";
import {blue, red} from "@material-ui/core/colors";
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import HomeIcon from '@material-ui/icons/Home';
import React from "react";
import {GameStateResponse} from '../../types';
import css from './style.module.scss';

export type CardType = 'blue' | 'red' | 'DANGER' | '' | null;

export type GameBoardContentProps = {
  gameState: GameStateResponse;
  onGuessWord(name: string): void;
};

function getCardColor(cardType: CardType, isOpen: boolean) {
  if (cardType == null) {
    return '#fff';
  }
  if (cardType === '' && isOpen) {
    return '#ffefb8';
  }
  return {
    red: red.A100,
    blue: blue.A100,
    DANGER: '#a1a1a1',
  }[cardType];
}

type ScoreItemProps = {
  team: 'red' | 'blue';
  userTeam: 'red' | 'blue';
  winner: 'red' | 'blue' | null;
  score: number;
  color: string;
}

function ScoreItem({team, userTeam, color, score, winner}: ScoreItemProps) {
  return <div className={css['score-item']}
              style={{backgroundColor: color}}>
    {userTeam == team ? <HomeIcon className={css['team-icon']}/> : null}
    {score}
    {winner == team ? <EmojiEventsIcon className={css['winner-icon']}/> : null}
  </div>
}

export default function GameBoardContent({gameState, onGuessWord}: GameBoardContentProps) {
  return (
    <Container maxWidth="md">
      <div className={`${css.row} ${css['score-row']}`}>
        <ScoreItem
          team={'red'}
          userTeam={gameState.me.team}
          score={gameState.remaining_word_count.red}
          color={red.A200}
          winner={gameState.winner}
        />
        <ScoreItem
          team={'blue'}
          userTeam={gameState.me.team}
          score={gameState.remaining_word_count.blue}
          color={blue.A200}
          winner={gameState.winner}
        />
      </div>
      <Grid container spacing={4}>
        {gameState.words.map((card) => (
          <Grid item xs={6} sm={3}>
            <Paper
              onClick={() => onGuessWord(card.name)}
              className={css['grid-paper']}
              style={{
                backgroundColor: getCardColor(card.team, card.is_open),
                opacity: card.is_open ? 0.5 : 1,
                cursor: card.is_open ? 'not-allowed' : 'pointer',
              }}
            >
              <span style={{textDecoration: card.is_open ? 'line-through' : 'none'}}>{card.name}</span>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}