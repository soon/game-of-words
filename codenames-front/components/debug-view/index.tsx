import React from 'react';
import css from './style.module.scss';

export type Message = {
  content: string;
  kind?: 'in' | 'out';
}

type DebugViewProps = {
  messages: Message[];
}

function getPrefix(message: Message): string {
  if (message.kind === 'in') {
    return '> ';
  }
  if (message.kind === 'out') {
    return '< ';
  }
  return '';
}

function fixUnicode(msg: string) {
  try {
    return JSON.stringify(JSON.parse(msg));
  } catch (e) {
    return msg;
  }
}

export default function DebugView({messages}: DebugViewProps) {
  return <div className={css.container}>
    {messages.map((m, idx) => (
      <div key={idx}>{getPrefix(m)}{fixUnicode(m.content)}</div>
    ))}
  </div>
}
