import {Button, Container, List, ListItem, ListItemText, TextField} from "@material-ui/core";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ReplayIcon from "@material-ui/icons/Replay";
import StarsIcon from '@material-ui/icons/Stars';
import * as React from 'react';
import {useEffect, useState} from 'react';
import {GameStateResponse} from '../../types';
import GradientBtn from "../start-game-btn";
import css from './style.module.scss';

export type TeamType = 'blue' | 'red';

export type LobbyContentProps = {
  gameState: GameStateResponse;

  onSetTeam(team: TeamType): void;
  onSetCaptain(isCaptain: boolean): void;
  onSetReady(isReady: boolean): void;
  onGenerateNewUsername(): void;
  name: string;
  onSetName(name: string): void;
  onStartGame(): void;
};

function getTeamColor(team) {
  if (team == null) {
    return null
  }
  return team === 'blue' ? '#2196f3' : '#ef5350';
}

export default function LobbyContent(
  {
    gameState, onSetTeam, onSetCaptain, onSetReady, onGenerateNewUsername, name, onSetName,
    onStartGame
  }: LobbyContentProps
) {
  const {me, players} = gameState;
  const [defaultName, setDefaultName] = useState<string | null>(null);

  useEffect(() => {
    if (defaultName == null && me.name) {
      setDefaultName(me.name);
      if (!name) {
        onSetName(me.name);
      }
    }
  }, [me.id, me.name])

  return (
    <Container maxWidth="md">
      <div className={css['lobby-container']}>
        <div className={css['row']}>
          <TextField
            className={css['user-name-text-field']} label="Ваше имя" variant="outlined" color='secondary'
            onChange={e => {
              const value = e.target.value;
              onSetName(value);
              if (value) {
                onSetName(e.target.value)
              }
            }}
            value={name}
          />
          <GradientBtn className={css['icon-button']} onClick={onGenerateNewUsername}><ReplayIcon/></GradientBtn>
        </div>
        <div className={`${css.row} ${css['second-row']}`}>
          <GradientBtn
            variant={me.is_captain ? 'text' : 'outlined'}
            onClick={() => onSetCaptain(!me.is_captain)}
            disabled={me.team == null}
          >
            Капитан
          </GradientBtn>
          <Button
            style={{
              backgroundColor: getTeamColor(me.team),
              color: me.team == null ? undefined : '#fff'
            }}
            color='primary'
            disabled={me.team == null}
            onClick={() => onSetTeam(me.team === 'blue' ? 'red' : 'blue')}
            variant={me.team == null ? 'outlined' : 'text'}
          >
            {me.team == null ? 'Тим' : me.team === 'blue' ? 'Тим Блу' : 'Тим Рэд'}
          </Button>
          <GradientBtn
            variant={me.is_ready ? 'text' : 'outlined'}
            disabled={me.team == null}
            onClick={() => onSetReady(!me.is_ready)}
          >
            Я готов
          </GradientBtn>
        </div>
        <List>
          {players.map((player, idx) => (
            <ListItem key={`${idx}-item`}>
              <div className={css['list-icon']} style={{backgroundColor: getTeamColor(player.team)}}/>
              <ListItemText primary={player.name}/>
              {player.is_captain && <StarsIcon className={css['status-icon']} color='secondary'/>}
              <CheckCircleIcon className={css['status-icon']} color={player.is_ready ? 'primary' : 'disabled'}/>
            </ListItem>
          ))}
        </List>
        {gameState.is_game_ready && <GradientBtn
          onClick={() => onStartGame()}
          className={css['start-game-btn']}
        >
          Начать игру
        </GradientBtn>}
      </div>
    </Container>
  );
}