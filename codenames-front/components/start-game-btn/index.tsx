import {Button, ButtonProps} from "@material-ui/core";
import React, {PropsWithChildren} from "react";
import css from './style.module.scss'

type GradientBtnProps = PropsWithChildren<{
  className?: string
} & ButtonProps>;

export default function GradientBtn({className, children, ...props}: GradientBtnProps) {
  const finalClassName = `${css.button} ${className || ''}`;
  return <Button
    className={finalClassName}
    color="primary"
    {...props}
  >
    {children}
  </Button>
}