require('dotenv').config();
module.exports = {
    env: {
        DEBUG: process.env.DEBUG === 'true',
        WS_URL: process.env.WS_URL,
    },
}