import os
import secrets
from pathlib import Path

from dotenv import load_dotenv


__all__ = ['REDIS_URL', 'REDIS_DB', 'SECRET']

load_dotenv()

REDIS_URL = os.getenv('REDIS_URL', default='redis://localhost')
REDIS_DB = int(os.getenv('REDIS_DB', default=13))

SCRIPT_DIR = Path(__file__).parent

KEY_PATH = SCRIPT_DIR / 'secret' / 'key'
if not KEY_PATH.exists():
    KEY_PATH.write_text(secrets.token_hex(64))

SECRET = KEY_PATH.read_text()
