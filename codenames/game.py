import random
from dataclasses import dataclass
from typing import Dict, List, Optional

from codenames.db import db, generate_user_id
from codenames.exceptions import ClientError, UserError, ValidationError
from codenames.words import NOUNS, USER_NAMES


GAME_STATUS_LOBBY = 'LOBBY'
GAME_STATUS_STARTED = 'STARTED'
GAME_STATUS_COMPLETED = 'COMPLETED'

TEAM_RED = 'red'
TEAM_BLU = 'blue'
TEAM_DANGER = 'DANGER'

OPPONENT_TEAM = {
    TEAM_RED: TEAM_BLU,
    TEAM_BLU: TEAM_RED,
}

GAME_SIZE = 24

FIRST_TEAM_WORDS = 9
SECOND_TEAM_WORDS = 8


@dataclass
class UserInfo:
    id: str
    name: Optional[str]
    team: str
    is_ready: bool
    is_captain: bool


@dataclass
class WordInfo:
    name: str
    team: Optional[str]
    is_open: bool


@dataclass
class GameInfo:
    id: str
    status: Optional[str]
    players: List[UserInfo]
    words: List[WordInfo]
    me: UserInfo
    is_game_ready: bool
    message: str
    winner: str
    team_score: Dict[str, int]
    remaining_word_count: Dict[str, int]


async def check_game_not_started(game_id: str):
    game_status = await db.get_game_status(game_id)
    if game_status is None:
        raise ClientError('Игра не найдена', 'game.not-found')
    if game_status != GAME_STATUS_LOBBY:
        raise UserError('Game already started')


async def check_game_is_running(game_id: str):
    if await db.get_game_status(game_id) != GAME_STATUS_STARTED:
        raise ValueError('Game is not started or already finished')


async def check_user_in_game(user_id: str, game_id: str):
    if not await db.is_user_in_game(user_id, game_id):
        raise ValueError('User not in game')


async def check_user_has_team(user_id: str, game_id: str):
    team = await db.get_user_team(user_id, game_id)
    if not team:
        raise ValueError('User not in team')
    return team


async def register_new_user():
    user_id = generate_user_id()
    while await db.is_user_exists(user_id):
        user_id = generate_user_id()
    await db.set_user_name(user_id, _generate_user_name())
    return user_id


async def new_game(user_id: str):
    game_id = await db.create_game(user_id)
    await db.set_game_status(game_id, GAME_STATUS_LOBBY)
    await connect_to_game(user_id, game_id)
    return game_id


async def set_user_name(user_id: str, name: str):
    try:
        return await db.set_user_name(user_id, name)
    except ValidationError:
        raise ClientError('Invalid name')


def _generate_user_name():
    return random.choice(USER_NAMES)


async def generate_new_user_name(user_id: str):
    current_name = await db.get_user_name(user_id)
    new_name = current_name
    while new_name == current_name:
        new_name = _generate_user_name()
    await db.set_user_name(user_id, new_name)
    return new_name


async def connect_to_game(user_id: str, game_id: str):
    if not await db.is_user_in_game(user_id, game_id):
        if await db.get_game_users_count(game_id) > 20:
            raise ClientError('Прости, дружище, игра уже переполнена')
        await check_game_not_started(game_id)
        await db.add_user_to_game(user_id, game_id)
    if not await db.get_user_team(user_id, game_id):
        await set_team(user_id, game_id, random.choice([TEAM_BLU, TEAM_RED]))


async def set_team(user_id: str, game_id: str, team: str):
    await check_game_not_started(game_id)
    await check_user_in_game(user_id, game_id)
    if team not in (TEAM_RED, TEAM_BLU):
        raise ClientError('Invalid team')
    user_team = await db.get_user_team(user_id, game_id)
    if user_team == team:
        return
    if user_team and await db.get_game_captain(game_id, user_team) == user_id:
        await db.remove_team_captain(game_id, user_team)
    await db.set_user_team(user_id, game_id, team)


async def apply_as_captain(user_id: str, game_id: str):
    await check_game_not_started(game_id)
    user_team = await check_user_has_team(user_id, game_id)
    if await db.get_game_captain(game_id, user_team):
        raise ClientError('В команде уже есть капитан')
    return await db.set_team_captain(user_id, game_id, user_team)


async def unapply_as_captain(user_id: str, game_id: str):
    await check_game_not_started(game_id)
    await check_user_in_game(user_id, game_id)
    user_team = await check_user_has_team(user_id, game_id)
    current_cap = await db.get_game_captain(game_id, user_team)
    if current_cap and current_cap != user_id:
        raise ClientError('Ты не капитан')

    return await db.remove_team_captain(game_id, user_team, )


async def ready(user_id: str, game_id: str):
    await check_game_not_started(game_id)
    await check_user_has_team(user_id, game_id)
    return await db.set_user_ready(user_id, game_id)


async def not_ready(user_id: str, game_id: str):
    await check_game_not_started(game_id)
    await check_user_has_team(user_id, game_id)
    return await db.set_user_ready(user_id, game_id, False)


def generate_words():
    return random.sample(NOUNS, GAME_SIZE)


async def start_game(user_id: str, game_id: str):
    await check_user_in_game(user_id, game_id)
    await check_game_not_started(game_id)
    game_info = await get_game_info(user_id, game_id)
    if not game_info.is_game_ready:
        raise ClientError(game_info.message)
    words = generate_words()
    words_indices = range(len(words))
    game_words_indices = random.sample(words_indices, FIRST_TEAM_WORDS + SECOND_TEAM_WORDS + 1)
    first_team_words_indices = game_words_indices[:FIRST_TEAM_WORDS]
    second_team_words_indices = game_words_indices[FIRST_TEAM_WORDS:FIRST_TEAM_WORDS + SECOND_TEAM_WORDS]
    danger_word_indices = game_words_indices[FIRST_TEAM_WORDS + SECOND_TEAM_WORDS:]
    teams = [TEAM_RED, TEAM_BLU]
    if random.random() >= 0.5:
        teams = teams[::-1]
    word_to_team = [''] * len(words)
    for idx in first_team_words_indices:
        word_to_team[idx] = teams[0]
    for idx in second_team_words_indices:
        word_to_team[idx] = teams[1]
    for idx in danger_word_indices:
        word_to_team[idx] = TEAM_DANGER
    await db.set_game_words(game_id, words)
    await db.set_game_words_teams(game_id, word_to_team)
    await db.set_game_guessed_words(game_id, [''] * len(words))
    await db.set_game_turn(game_id, teams[0])
    await db.set_game_status(game_id, GAME_STATUS_STARTED)


async def get_winner(game_id: str):
    words_teams = await db.get_game_words_teams(game_id)
    words_statuses = await db.get_game_guessed_words(game_id)
    for team in (TEAM_RED, TEAM_BLU):
        team_wins = all(s for (t, s) in zip(words_teams, words_statuses) if t == team)
        if team_wins:
            return team


async def guess_word(user_id: str, game_id: str, word: str):
    await check_game_is_running(game_id)
    await check_user_in_game(user_id, game_id)

    word_state = await db.get_word_state(game_id, word)
    if word_state:
        raise ClientError('Слово уже открыто')
    user_team = await db.get_user_team(user_id, game_id)
    if await db.get_game_captain(game_id, user_team) == user_id:
        raise ClientError('Ты капитан, ты сильный и смелый')

    await db.set_word_guessed(game_id, word)

    word_team = await db.get_word_team(game_id, word)
    if word_team == TEAM_DANGER:
        winner = OPPONENT_TEAM[user_team]
    else:
        winner = await get_winner(game_id)

    if winner:
        await db.set_winner(game_id, winner)
        await db.set_game_status(game_id, GAME_STATUS_COMPLETED)


async def get_game_info(user_id: str, game_id: str):
    game_status = await db.get_game_status(game_id)
    if not game_status:
        raise ValueError('Game not found')
    game_status = await db.get_game_status(game_id)
    user_ids = await db.get_game_users(game_id)
    users = []
    for uid in user_ids:
        team = await db.get_user_team(uid, game_id)
        name = await db.get_user_name(uid)
        is_ready = await db.is_user_ready(uid, game_id)
        is_captain = team and uid == await db.get_game_captain(game_id, team)
        users.append(UserInfo(
            uid,
            name,
            team,
            is_ready,
            is_captain,
        ))
    users = sorted(users, key=lambda x: (x.team, not x.is_captain, x.name, x.id))
    me_list = [x for x in users if x.id == user_id]
    if me_list:
        me = me_list[0]
    else:
        me = UserInfo(
            user_id,
            name=await db.get_user_name(user_id),
            team=None,
            is_ready=False,
            is_captain=False,
        )
    user_team = await db.get_user_team(user_id, game_id)
    is_captain = user_team and user_id == await db.get_game_captain(game_id, user_team)
    raw_words = await db.get_game_words(game_id)
    words = []
    team_score = {
        TEAM_BLU: 0,
        TEAM_RED: 0,
    }
    remaining_word_count = {
        TEAM_BLU: 0,
        TEAM_RED: 0,
    }
    for idx, w in enumerate(raw_words):
        word_state = await db.is_word_guessed(game_id, idx)
        word_team = await db.get_word_team_by_index(game_id, idx)
        if word_team in team_score and word_state:
            team_score[word_team] += 1
        if word_team in remaining_word_count and not word_state:
            remaining_word_count[word_team] += 1
        words.append(WordInfo(
            w,
            word_team if is_captain or word_state or game_status == GAME_STATUS_COMPLETED else None,
            word_state,
        ))

    team_to_users = {TEAM_BLU: [], TEAM_RED: []}
    team_cap = {TEAM_BLU: False, TEAM_RED: False}
    team_ready = {TEAM_BLU: True, TEAM_RED: True}
    for u in users:
        if u.is_captain:
            team_cap[u.team] = True
        team_ready[u.team] &= u.is_ready
        team_to_users[u.team].append(u)

    game_ready = False
    msg = ''
    if len(team_to_users[TEAM_BLU]) < 2 and len(team_to_users[TEAM_RED]) < 2:
        msg = 'Команды должны состоять минимум из двух игроков'
    elif len(team_to_users[TEAM_BLU]) < 2:
        msg = 'В команде синих должно быть минимум два игрока'
    elif len(team_to_users[TEAM_RED]) < 2:
        msg = 'В команде красных должно быть минимум два игрока'
    elif not team_cap[TEAM_BLU] and not team_cap[TEAM_RED]:
        msg = 'Команды должны выбрать капитанов'
    elif not team_cap[TEAM_BLU]:
        msg = 'Нет капитана синей команды'
    elif not team_cap[TEAM_RED]:
        msg = 'Нет капитана красной команды'
    elif not team_ready[TEAM_BLU] and not team_ready[TEAM_RED]:
        msg = 'Игроки команд не готовы'
    elif not team_ready[TEAM_BLU]:
        msg = 'Игроки синей команды не готовы'
    elif not team_ready[TEAM_RED]:
        msg = 'Игроки красной команды не готовы'
    else:
        game_ready = True
    winner = await db.get_game_winner(game_id)
    return GameInfo(
        game_id,
        game_status,
        users,
        words,
        me,
        game_ready,
        msg,
        winner,
        team_score,
        remaining_word_count,
    )
