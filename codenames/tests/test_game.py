import random

import pytest

from codenames import game
from codenames.db import db


@pytest.fixture
async def init_db(event_loop):
    await db.init('redis://localhost', db=2)
    await db.redis.flushdb()
    yield
    await db.close()


@pytest.fixture
def init_random():
    random.seed(12345)


@pytest.mark.usefixtures('init_db', 'init_random')
@pytest.mark.asyncio
class TestGame:
    async def test_new_game(self):
        game_id = await game.new_game('user')
        assert game_id is not None

    async def test_set_user_name(self):
        await game.set_user_name('user', 'new name')
        name = await db.get_user_name('user')
        assert name == 'new name'

    async def test_connect_to_game(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        assert await db.is_user_in_game('new-user', game_id)
        assert await db.is_user_ready('new-user', game_id) is False

    async def test_set_team(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_BLU)
        assert await db.get_user_team('new-user', game_id) == game.TEAM_BLU

    async def test_apply_as_captain(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_BLU)
        await game.apply_as_captain('new-user', game_id)
        assert await db.get_game_captain(game_id, game.TEAM_BLU) == 'new-user'
        assert await db.get_game_captain(game_id, game.TEAM_RED) is None

    async def test_team_change_removes_captain(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_BLU)
        await game.apply_as_captain('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_RED)
        assert await db.get_game_captain(game_id, game.TEAM_BLU) is None
        assert await db.get_game_captain(game_id, game.TEAM_RED) is None

    async def test_unapply_as_captain(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_RED)
        await game.apply_as_captain('new-user', game_id)
        await game.unapply_as_captain('new-user', game_id)
        assert await db.get_game_captain(game_id, game.TEAM_BLU) is None
        assert await db.get_game_captain(game_id, game.TEAM_RED) is None

    async def test_ready(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_RED)
        await game.ready('new-user', game_id)
        assert await db.is_user_ready('new-user', game_id) is True

    async def test_not_ready(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('new-user', game_id)
        await game.set_team('new-user', game_id, game.TEAM_BLU)
        await game.ready('new-user', game_id)
        await game.not_ready('new-user', game_id)
        assert await db.is_user_ready('new-user', game_id) is False

    async def test_complete_game(self):
        game_id = await game.new_game('user')

        await game.connect_to_game('blu-cap', game_id)
        await game.set_team('blu-cap', game_id, game.TEAM_BLU)
        await game.apply_as_captain('blu-cap', game_id)
        await game.ready('blu-cap', game_id)

        await game.connect_to_game('blu-user', game_id)
        await game.set_team('blu-user', game_id, game.TEAM_BLU)
        await game.ready('blu-user', game_id)

        await game.connect_to_game('red-cap', game_id)
        await game.set_team('red-cap', game_id, game.TEAM_RED)
        await game.apply_as_captain('red-cap', game_id)
        await game.ready('red-cap', game_id)

        await game.connect_to_game('red-user', game_id)
        await game.set_team('red-user', game_id, game.TEAM_RED)
        await game.ready('red-user', game_id)

        await game.ready('user', game_id)

        await game.start_game('user', game_id)
        await db.set_game_turn(game_id, game.TEAM_RED)
        words = await db.get_game_words(game_id)
        teams = await db.get_game_words_teams(game_id)
        red_words = [x for (x, t) in zip(words, teams) if t == game.TEAM_RED]
        for w in red_words:
            await game.guess_word('red-user', game_id, w)
        assert await db.get_game_status(game_id) == game.GAME_STATUS_COMPLETED
        assert await db.get_game_winner(game_id) == game.TEAM_RED

    async def test_get_game_info_by_non_existing_id(self):
        with pytest.raises(ValueError) as exc:
            await game.get_game_info('user', 'game')
        assert str(exc.value) == 'Game not found'

    async def test_lobby_game_info(self):
        game_id = await game.new_game('user')
        game_info = await game.get_game_info('user', game_id)
        assert game_info.status == game.GAME_STATUS_LOBBY
        assert game_info.players == [
            game.UserInfo(
                id='user',
                name=None,
                team='red',
                is_ready=False,
                is_captain=False,
            )
        ]
        assert game_info.words == []

    async def test_lobby_game_info_with_users(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('blu-cap', game_id)
        await game.set_team('blu-cap', game_id, game.TEAM_BLU)
        await game.apply_as_captain('blu-cap', game_id)
        await game.ready('blu-cap', game_id)

        await game.connect_to_game('blu-user', game_id)
        await game.set_team('blu-user', game_id, game.TEAM_BLU)

        await game.connect_to_game('red-cap', game_id)
        await game.set_team('red-cap', game_id, game.TEAM_RED)
        await game.apply_as_captain('red-cap', game_id)

        await game.connect_to_game('red-user', game_id)
        await game.set_team('red-user', game_id, game.TEAM_RED)
        await game.ready('red-user', game_id)

        game_info = await game.get_game_info('user', game_id)

        assert game_info.status == game.GAME_STATUS_LOBBY
        assert game_info.players == [
            game.UserInfo(id='blu-cap', name=None, team='blue', is_ready=True, is_captain=True),
            game.UserInfo(id='blu-user', name=None, team='blue', is_ready=False, is_captain=False),
            game.UserInfo(id='red-cap', name=None, team='red', is_ready=False, is_captain=True),
            game.UserInfo(id='red-user', name=None, team='red', is_ready=True, is_captain=False),
            game.UserInfo(id='user', name=None, team='red', is_ready=False, is_captain=False)
        ]
        assert game_info.words == []

    async def test_started_game(self):
        game_id = await game.new_game('user')
        await game.connect_to_game('blu-cap', game_id)
        await game.set_team('blu-cap', game_id, game.TEAM_BLU)
        await game.apply_as_captain('blu-cap', game_id)
        await game.ready('blu-cap', game_id)

        await game.connect_to_game('blu-user', game_id)
        await game.set_team('blu-user', game_id, game.TEAM_BLU)
        await game.ready('blu-user', game_id)

        await game.connect_to_game('red-cap', game_id)
        await game.set_team('red-cap', game_id, game.TEAM_RED)
        await game.apply_as_captain('red-cap', game_id)
        await game.ready('red-cap', game_id)

        await game.connect_to_game('red-user', game_id)
        await game.set_team('red-user', game_id, game.TEAM_RED)
        await game.ready('red-user', game_id)

        await game.ready('user', game_id)

        await game.start_game('user', game_id)
        await db.set_game_turn(game_id, game.TEAM_RED)
        old_words = await db.get_game_words(game_id)
        words = [f'word-{idx}' for idx in range(len(old_words))]
        await db.set_game_words(game_id, words)

        game_info = await game.get_game_info('user', game_id)

        assert game_info.status == game.GAME_STATUS_STARTED
        assert game_info.players == [
            game.UserInfo(id='blu-cap', name=None, team='blue', is_ready=True, is_captain=True),
            game.UserInfo(id='blu-user', name=None, team='blue', is_ready=True, is_captain=False),
            game.UserInfo(id='red-cap', name=None, team='red', is_ready=True, is_captain=True),
            game.UserInfo(id='red-user', name=None, team='red', is_ready=True, is_captain=False),
            game.UserInfo(id='user', name=None, team='red', is_ready=True, is_captain=False),
        ]
        assert game_info.words == [
            game.WordInfo(name=w, team=None, is_open=False) for w in words
        ]

        cap_game_info = await game.get_game_info('red-cap', game_id)
        words_teams = await db.get_game_words_teams(game_id)

        assert any(x for x in words_teams)
        assert cap_game_info.status == game_info.status
        assert cap_game_info.players == game_info.players
        assert cap_game_info.words == [
            game.WordInfo(name=w, team=t, is_open=False) for w, t in zip(words, words_teams)
        ]
