import functools
import inspect
import re
import types
import uuid
from typing import List

import aioredis

from codenames.exceptions import ValidationError


GAME_ID_RE = re.compile(r'^[a-zA-Z\d-]{1,64}$')
USER_ID_RE = re.compile(r'^[a-zA-Z\d-]{1,64}$')
TEAM_NAME_RE = re.compile(r'^[a-zA-Z\d-]{1,64}$')
REGULAR_STRING_RE = re.compile(r'^.{1,100}$')


def validate_game_id(game_id):
    if not GAME_ID_RE.match(game_id):
        raise ValueError('invalid game id')


def generate_user_id():
    return str(uuid.uuid4())


def validate_user_id(user_id):
    if not USER_ID_RE.match(user_id):
        raise ValueError('invalid user id')


def validate_team(team):
    if not TEAM_NAME_RE.match(team):
        raise ValueError('invalid team name')


GAME_KEY_TEMPLATE = 'game:{}'
GAME_STATUS_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:status'
GAME_OWNER_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:owner'
GAME_USERS_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:users'
GAME_TURN_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:turn'
GAME_WINNER_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:winner'
GAME_USER_TEAM_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:user:{{}}:team'
GAME_USER_READY_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:user:{{}}:ready'
GAME_TEAM_CAPTAIN_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:team:{{}}:captain'
GAME_WORDS_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:words'
GAME_WORDS_TEAMS_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:words-teams'
GAME_GUESSED_WORDS_KEY_TEMPLATE = f'{GAME_KEY_TEMPLATE}:guessed-words'

USER_KEY_TEMPLATE = 'user:{}'
USER_NAME_KEY_TEMPLATE = f'{USER_KEY_TEMPLATE}:name'


def validate_params(func):
    signature = inspect.signature(func)
    validators = {
        'game_id': validate_game_id,
        'user_id': validate_user_id,
        'team': validate_team,
    }

    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        bound_arguments = signature.bind_partial(*args, **kwargs)
        for name, value in bound_arguments.arguments.items():
            if name in validators:
                validators[name](value)
        return await func(*args, **kwargs)

    return wrapper


class ValidatorMeta(type):
    def __new__(mcs, name, bases, attrs):
        for attr_name, attr_value in attrs.items():
            if isinstance(attr_value, types.FunctionType) and attr_name != 'init' and not attr_name.startswith('__'):
                attrs[attr_name] = validate_params(attr_value)

        return super().__new__(mcs, name, bases, attrs)


class Db(metaclass=ValidatorMeta):
    def __init__(self):
        self.redis = None

    async def init(self, url, db=None):
        self.redis = await aioredis.create_redis(url, db=db, encoding='utf-8')

    async def close(self):
        if self.redis:
            self.redis.close()
            await self.redis.wait_closed()
        self.redis = None

    async def get_game_status(self, game_id):
        return await self.redis.get(GAME_STATUS_KEY_TEMPLATE.format(game_id))

    async def set_game_status(self, game_id, state):
        await self.redis.set(GAME_STATUS_KEY_TEMPLATE.format(game_id), state)

    async def create_game(self, user_id):
        game_id = str(uuid.uuid4())
        await self.redis.set(GAME_OWNER_KEY_TEMPLATE.format(game_id), user_id)
        return game_id

    async def set_user_name(self, user_id, name):
        if not REGULAR_STRING_RE.match(name):
            raise ValidationError('invalid name')
        await self.redis.set(USER_NAME_KEY_TEMPLATE.format(user_id), name)

    async def get_user_name(self, user_id):
        return await self.redis.get(USER_NAME_KEY_TEMPLATE.format(user_id))

    async def add_user_to_game(self, user_id, game_id):
        await self.redis.sadd(GAME_USERS_KEY_TEMPLATE.format(game_id), user_id)

    async def set_user_team(self, user_id, game_id, team):
        if not REGULAR_STRING_RE.match(team):
            raise ValidationError('invalid team')
        await self.redis.set(GAME_USER_TEAM_KEY_TEMPLATE.format(game_id, user_id), team)

    async def is_user_in_game(self, user_id, game_id):
        return await self.redis.sismember(GAME_USERS_KEY_TEMPLATE.format(game_id), user_id)

    async def get_game_captain(self, game_id, team):
        return await self.redis.get(GAME_TEAM_CAPTAIN_KEY_TEMPLATE.format(game_id, team))

    async def set_team_captain(self, user_id, game_id, team):
        await self.redis.set(GAME_TEAM_CAPTAIN_KEY_TEMPLATE.format(game_id, team), user_id)

    async def remove_team_captain(self, game_id, team):
        await self.redis.delete(GAME_TEAM_CAPTAIN_KEY_TEMPLATE.format(game_id, team))

    async def set_user_ready(self, user_id, game_id, apply=True):
        if apply:
            await self.redis.set(GAME_USER_READY_KEY_TEMPLATE.format(game_id, user_id), 1)
        else:
            await self.redis.delete(GAME_USER_READY_KEY_TEMPLATE.format(game_id, user_id))

    async def get_game_users(self, game_id):
        return await self.redis.smembers(GAME_USERS_KEY_TEMPLATE.format(game_id))

    async def get_game_users_count(self, game_id):
        return await self.redis.scard(GAME_USERS_KEY_TEMPLATE.format(game_id))

    async def get_ready_game_users(self, game_id):
        return {x for x in await self.get_game_users(game_id) if await self.is_user_ready(x, game_id)}

    async def is_user_ready(self, user_id, game_id):
        return bool(await self.redis.get(GAME_USER_READY_KEY_TEMPLATE.format(game_id, user_id)))

    async def get_team_users(self, game_id, team):
        return {x for x in await self.get_game_users(game_id) if await self.get_user_team(x, game_id) == team}

    async def get_user_team(self, user_id, game_id):
        return await self.redis.get(GAME_USER_TEAM_KEY_TEMPLATE.format(game_id, user_id))

    async def set_game_words(self, game_id, words):
        await self.redis.ltrim(GAME_WORDS_KEY_TEMPLATE.format(game_id), 1, 0)
        await self.redis.rpush(GAME_WORDS_KEY_TEMPLATE.format(game_id), *words)

    async def get_game_words(self, game_id) -> List[str]:
        return await self.redis.lrange(GAME_WORDS_KEY_TEMPLATE.format(game_id), 0, -1)

    async def set_game_words_teams(self, game_id, teams):
        await self.redis.ltrim(GAME_WORDS_TEAMS_KEY_TEMPLATE.format(game_id), 1, 0)
        await self.redis.rpush(GAME_WORDS_TEAMS_KEY_TEMPLATE.format(game_id), *teams)

    async def get_game_words_teams(self, game_id):
        return await self.redis.lrange(GAME_WORDS_TEAMS_KEY_TEMPLATE.format(game_id), 0, -1)

    async def set_game_guessed_words(self, game_id, status):
        await self.redis.ltrim(GAME_GUESSED_WORDS_KEY_TEMPLATE.format(game_id), 1, 0)
        await self.redis.rpush(GAME_GUESSED_WORDS_KEY_TEMPLATE.format(game_id), *status)

    async def get_game_guessed_words(self, game_id):
        return await self.redis.lrange(GAME_GUESSED_WORDS_KEY_TEMPLATE.format(game_id), 0, -1)

    async def set_game_turn(self, game_id, team):
        await self.redis.set(GAME_TURN_KEY_TEMPLATE.format(game_id), team)

    async def get_game_turn(self, game_id):
        return await self.redis.get(GAME_TURN_KEY_TEMPLATE.format(game_id))

    async def get_word_index(self, game_id, word):
        words = await self.get_game_words(game_id)
        try:
            return words.index(word)
        except ValueError:
            raise ValueError('word not found')

    async def get_word_state(self, game_id, word=None):
        idx = await self.get_word_index(game_id, word)
        return await self.is_word_guessed(game_id, idx)

    async def is_word_guessed(self, game_id, idx):
        return bool(await self.redis.lindex(GAME_GUESSED_WORDS_KEY_TEMPLATE.format(game_id), idx))

    async def set_word_guessed(self, game_id, word):
        idx = await self.get_word_index(game_id, word)
        await self.redis.lset(GAME_GUESSED_WORDS_KEY_TEMPLATE.format(game_id), idx, 1)

    async def get_word_team(self, game_id, word):
        idx = await self.get_word_index(game_id, word)
        return await self.get_word_team_by_index(game_id, idx)

    async def get_word_team_by_index(self, game_id, idx):
        return await self.redis.lindex(GAME_WORDS_TEAMS_KEY_TEMPLATE.format(game_id), idx)

    async def set_winner(self, game_id, team):
        await self.redis.set(GAME_WINNER_KEY_TEMPLATE.format(game_id), team)

    async def get_game_winner(self, game_id):
        return await self.redis.get(GAME_WINNER_KEY_TEMPLATE.format(game_id))

    async def is_user_exists(self, user_id):
        return await self.redis.exists(USER_NAME_KEY_TEMPLATE.format(user_id))


db: Db = Db()
