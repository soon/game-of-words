#!/usr/bin/env python3

import asyncclick as click

from codenames import game, settings
from codenames.db import db


@click.group()
async def cli():
    await db.init(settings.REDIS_URL, db=settings.REDIS_DB)


@cli.command()
async def create_user():
    user_id = await game.register_new_user()
    click.echo(user_id)


@cli.command()
@click.argument("game_id")
@click.argument("count", default=1)
async def add_new_user_to_game(game_id, count):
    for i in range(count):
        user_id = await game.register_new_user()
        await game.connect_to_game(user_id, game_id)
    game_info = await game.get_game_info('anonymous', game_id)
    click.echo(game_info)


@cli.command()
@click.argument("game_id")
async def make_all_game_users_ready(game_id):
    users = await db.get_game_users(game_id)
    for u in users:
        await game.ready(u, game_id)
    game_info = await game.get_game_info('anonymous', game_id)
    click.echo(game_info)


@cli.command()
@click.argument("game_id")
@click.argument("team")
async def set_team_cap(game_id, team):
    game_info = await game.get_game_info('anonymous', game_id)
    for u in game_info.players:
        if u.team == team:
            await game.apply_as_captain(u.id, game_id)
            game_info = await game.get_game_info('anonymous', game_id)
            click.echo(game_info)
            return
    click.echo('No users in this team')


@cli.command()
@click.argument("game_id")
async def prepare_game(game_id):
    game_info = await game.get_game_info('anonymous', game_id)
    team_info = {
        game.TEAM_RED: {
            'need_cap': True,
            'players_count': 0,
        },
        game.TEAM_BLU: {
            'need_cap': True,
            'players_count': 0,
        },
    }
    for p in game_info.players:
        if p.is_captain:
            team_info[p.team]['need_cap'] = False
        team_info[p.team]['players_count'] += 1
        await game.ready(p.id, game_id)

    for team in team_info:
        while team_info[team]['players_count'] < 2:
            user_id = await game.register_new_user()
            await game.connect_to_game(user_id, game_id)
            await game.set_team(user_id, game_id, team)
            team_info[team]['players_count'] += 1

            if team_info[team]['need_cap']:
                await game.apply_as_captain(user_id, game_id)
                team_info[team]['need_cap'] = False

            await game.ready(user_id, game_id)


if __name__ == '__main__':
    cli(_anyio_backend='asyncio')
