import asyncio
import json
import logging
import time
from collections import defaultdict
from dataclasses import asdict

import jwt
import websockets

from codenames import game, settings
from codenames.db import db, validate_user_id
from codenames.exceptions import ClientError


logger = logging.getLogger(__name__)

user_id_to_clients = defaultdict(set)
client_to_user_id = {}
game_id_to_clients = defaultdict(set)
client_to_game_ids = defaultdict(set)

game_id_to_start_handlers = {}


def require_user_id(websocket):
    user_id = client_to_user_id.get(websocket)
    if not user_id:
        raise ClientError('Auth firstly')
    return user_id


def require_game_id(payload):
    game_id = payload.get('game_id')
    if not game_id:
        raise ClientError('game_id required')
    return game_id


async def reply(response, ws, request):
    response = response.copy()
    if request.get('action'):
        response['action'] = request['action']
    await ws.send(json.dumps(response))


def bind_user_id_to_client(user_id, ws):
    client_to_user_id[ws] = user_id
    user_id_to_clients[user_id].add(ws)


async def handle_auth(payload, websocket, path):
    if 'user_id' not in payload:
        raise ClientError('user_id is required')
    signed_token = payload['user_id']
    try:
        token = jwt.decode(signed_token, settings.SECRET, algorithms=['HS256'])
    except jwt.InvalidTokenError:
        return await handle_new_user({'action': 'new-user'}, websocket, path)
    user_id = token['id']
    try:
        validate_user_id(user_id)
    except ValueError:
        raise ClientError('Invalid user_id')
    bind_user_id_to_client(user_id, websocket)
    await reply({'status': 'ok'}, websocket, payload)


async def handle_new_user(payload, websocket: websockets.WebSocketServerProtocol, path):
    user_id = await game.register_new_user()
    encoded_user_id = jwt.encode({
        'id': user_id,
        'iat': int(time.time())
    }, settings.SECRET, algorithm='HS256')
    bind_user_id_to_client(user_id, websocket)
    await reply({'user_id': encoded_user_id.decode('utf-8')}, websocket, payload)


async def send_game_info(websocket, game_info, action='game-update'):
    await websocket.send(json.dumps({
        'action': action,
        'game': asdict(game_info)
    }))


async def handle_new_game(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = await game.new_game(user_id)
    game_id_to_clients[game_id].add(websocket)
    client_to_game_ids[websocket].add(game_id)

    copy_game_id = payload.get('copy')
    if copy_game_id:
        game_users = await db.get_game_users(copy_game_id)
        for u in game_users:
            await game.connect_to_game(u, game_id)
            user_clients = user_id_to_clients[u]
            game_id_to_clients[game_id].update(user_clients)
            for c in user_clients:
                client_to_game_ids[c].add(game_id)

    notify_game_info_change(game_id, 'new-game')


async def handle_connect_to_game(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    await game.connect_to_game(user_id, game_id)
    game_id_to_clients[game_id].add(websocket)
    client_to_game_ids[websocket].add(game_id)
    game_info = await game.get_game_info(user_id, game_id)
    await send_game_info(websocket, game_info, action='connect-to-game')
    notify_game_info_change(game_id)


async def handle_generate_new_username(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    new_name = await game.generate_new_user_name(user_id)
    await reply({'name': new_name}, websocket, payload)
    notify_game_info_change(game_id)


async def handle_set_team(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    team = payload.get('team')
    await game.set_team(user_id, game_id, team)
    await reply({'team': team}, websocket, payload)
    notify_game_info_change(game_id)


async def handle_set_name(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    name = payload.get('name')
    if not name:
        raise ClientError('Invalid name')
    await game.set_user_name(user_id, name)
    notify_game_info_change(game_id)


async def handle_set_captain(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    is_captain = payload.get('is_captain')
    if 'is_captain' not in payload:
        raise ClientError('is_captain is required')
    if is_captain:
        await game.apply_as_captain(user_id, game_id)
    else:
        await game.unapply_as_captain(user_id, game_id)
    notify_game_info_change(game_id)


async def handle_set_ready(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    is_ready = payload.get('is_ready')
    if 'is_ready' not in payload:
        raise ClientError('is_ready is required')
    if is_ready:
        await game.ready(user_id, game_id)
    else:
        await game.not_ready(user_id, game_id)
    notify_game_info_change(game_id)


async def handle_start_game(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    await game.start_game(user_id, game_id)
    notify_game_info_change(game_id)


async def handle_guess_word(payload, websocket, path):
    user_id = require_user_id(websocket)
    game_id = require_game_id(payload)
    word = payload.get('word')
    if not word:
        raise ClientError('word is required')
    await game.guess_word(user_id, game_id, word)
    notify_game_info_change(game_id)


action_handlers = {
    'auth': handle_auth,
    'new-user': handle_new_user,
    'new-game': handle_new_game,
    'connect-to-game': handle_connect_to_game,
    'generate-new-name': handle_generate_new_username,
    'set-team': handle_set_team,
    'set-name': handle_set_name,
    'set-captain': handle_set_captain,
    'set-ready': handle_set_ready,
    'start-game': handle_start_game,
    'guess-word': handle_guess_word,
}


async def do_notify_game_info_change(game_id, client: websockets.WebSocketServerProtocol, action):
    user_id = client_to_user_id.get(client)
    if not user_id:
        return
    game_info = await game.get_game_info(user_id, game_id)
    await client.send(json.dumps({
        'action': action,
        'game': asdict(game_info)
    }))


async def notify_game_info_change_async(game_id, action):
    clients = game_id_to_clients[game_id]
    await asyncio.gather(*[
        do_notify_game_info_change(game_id, c, action)
        for c in clients
    ])


def notify_game_info_change(game_id, action='game-update'):
    asyncio.ensure_future(notify_game_info_change_async(game_id, action))


async def handle_message(message, websocket: websockets.WebSocketServerProtocol, path: str):
    try:
        data = json.loads(message)
    except Exception:
        raise ClientError('Invalid format')
    if not isinstance(data, dict):
        raise ClientError('Invalid format')
    action = data.get('action')
    if not action:
        raise ClientError('Action not specified')
    if action not in action_handlers:
        raise ClientError('Invalid action')
    await action_handlers[action](data, websocket, path)


def unregister_client(websocket):
    for g in client_to_game_ids[websocket]:
        game_id_to_clients[g].discard(websocket)
    client_to_game_ids.pop(websocket, None)
    for u in client_to_user_id:
        user_id_to_clients[u].discard(websocket)
    client_to_user_id.pop(websocket, None)


async def main_handler(websocket: websockets.WebSocketServerProtocol, path: str):
    client_to_user_id.pop(websocket, None)
    try:
        async for message in websocket:
            try:
                await handle_message(message, websocket, path)
            except ClientError as exc:
                logger.exception('Client error')
                await websocket.send(json.dumps(exc.as_dict()))
            except Exception:
                logger.exception('Internal error')
                await websocket.send(json.dumps({
                    'status': 'error',
                    'kind': 'server',
                    'message': 'Internal error',
                }))
    finally:
        unregister_client(websocket)


async def server():
    await db.init(settings.REDIS_URL, db=settings.REDIS_DB)
    await websockets.serve(main_handler, '0.0.0.0', 8765)
    print('Server started.')


def main():
    asyncio.get_event_loop().run_until_complete(server())
    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    main()
