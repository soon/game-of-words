class ClientError(Exception):
    kind = 'client'

    def __init__(self, message, code='error'):
        self.message = message
        self.code = code

    def as_dict(self):
        return {
            'status': 'error',
            'kind': self.kind,
            'message': self.message,
            'code': self.code
        }


class UserError(ClientError):
    kind = 'user'


class ValidationError(Exception):
    pass
