# Demo

https://woobdyboobdy.xyz/

# Deploy

1. Create codenames-front/.env.prod
2. Run docker-compose up
3. Start nginx on host with `/etc/nginx/sites-enabled/default` like this:

       map $http_upgrade $connection_upgrade {
           default upgrade;
           ''      close;
       }
       
       upstream game {
           server localhost:8080;
       }
       
       server {
           server_name server.name;
           proxy_http_version 1.1;
           proxy_set_header Upgrade $http_upgrade;
           proxy_set_header Connection $connection_upgrade;
           proxy_set_header Host $host;
       
           location / {
               proxy_pass http://game;
           }
       }
