FROM python:3.8-slim
RUN pip install poetry

WORKDIR /usr/src/app

COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false && poetry install

COPY codenames ./codenames
ENV PYTHONPATH "${PYTHONPATH}:/usr/src/app"
VOLUME /usr/src/app/codenames/secret

EXPOSE 8765
ENTRYPOINT ["python"]
CMD ["codenames/app.py"]
